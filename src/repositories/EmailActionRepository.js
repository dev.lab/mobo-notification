import models from "../models";
import { NotFound } from "../errors";

export default class EmailActionRepository {
  static async getTenantEmailTemplate({ tenantId, templateType }) {
    const data = await models.EmailAction.findOne({
      where: {
        tenant_id: tenantId,
        type: templateType,
      },
      include: [
        {
          model: models.Contact,
        },
      ],
    });

    if (!data) {
      throw new NotFound("Template message was not found.");
    }
    return data.toJSON();
  }
}
