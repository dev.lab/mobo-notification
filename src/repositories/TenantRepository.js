import models from "../models";

export default class TenantRepository {
  static async getByUuid({ tenantUuid }) {
    return models.Tenant.findOne({
      where: {
        uuid: tenantUuid
      }
    });
  }
}
