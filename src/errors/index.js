import ExternalDependency from "./ExternalDependency";
import Forbidden from "./Forbidden";
import NotFound from "./NotFound";

export { ExternalDependency, Forbidden, NotFound };
