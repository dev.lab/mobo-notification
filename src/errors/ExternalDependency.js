export default class ExternalDependency extends Error {
  constructor(externalError, message, errorCode) {
    super();
    Error.captureStackTrace(this, this.constructor);

    this.message = message || "The server couldn't reach external dependency";
    this.code = errorCode || 422;
    this.externalError = externalError;
  }
}
