export default class Forbidden extends Error {
  constructor(message, errorCode) {
    super();
    Error.captureStackTrace(this, this.constructor);

    this.message = message || "You are not authorized to access this data.";
    this.code = errorCode || 403;
  }
}
