export default class NotFound extends Error {
  constructor(message, errorCode) {
    super();
    Error.captureStackTrace(this, this.constructor);

    this.message = message || "The requested resource couldn't be found";
    this.code = errorCode || 404;
  }
}
