export default function(data) {
  return data.resource.items
    .map(item => {
      if (item.type === "application/vnd.lime.select+json") {
        item.content = item.content.text;
      }

      if (item.type === "application/vnd.lime.collection+json") {
        item.content = item.content.items[0].header.value.title;
      }

      if (item.type === "application/json") {
        item.content = item.metadata["#blip.payload.content"];
      }

      return item;
    })
    .filter(item => typeof item.content === "string")
    .map(item =>
      item.direction === "sent"
        ? "Mobo: " + item.content
        : "Cliente: " + item.content
    )
    .reverse();
}
