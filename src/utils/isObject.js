/**
 * Check if a value is an object
 * @param o
 * @returns {boolean}
 */
export default (o) => o === Object(o) && !Array.isArray(o) && typeof o !== 'function'
