export default function (template, variables) {
    let text = template;
    for(const prop in variables) {
        const templateVarRegexp = new RegExp('(\\${('+ prop +'(:-)?)})','g');
        text = text.replace(templateVarRegexp, variables[prop]);
    }
    return text;
}
