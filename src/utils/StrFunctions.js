/**
 * Add leading zeros to a integer
 * @param num
 * @param size
 * @returns {string}
 */
export function pad(num, size) {
  let s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}

/**
 * Transform any pattern to camel case
 * @param s
 * @returns {*|void|string}
 */
export function toCamel(s) {
  return s.replace(/([-_][a-z])/gi, $1 => {
    return $1
      .toUpperCase()
      .replace("-", "")
      .replace("_", "");
  });
}

/**
 * Format an 8 digit string to a valid CPF format
 * @param {*} cpf
 * @returns {string}
 */
export function formatCPF(cpf) {
  return cpf
    .replace(/[^\d]/g, "")
    .replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
}

/**
 * Return string only if it's not null
 * @param {string} str
 */
export function checkNull(str) {
  return str || "";
}
