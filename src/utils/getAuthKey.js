import md5 from 'crypto-js/md5'
import {pad} from './StrFunctions'

/**
 * Util function that generates Cani access token.
 *
 * @returns
 */
export default function getAuthKey () {
  const date = new Date()
  const dayString = pad(date.getDate(), 2)
  const monthString = pad(date.getMonth() + 1, 2)
  const key = 'cianet' + dayString + monthString
  return md5(key).toString()
}
