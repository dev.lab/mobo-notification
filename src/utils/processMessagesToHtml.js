const styles = `<style> div {box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;} .partner {box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;}.msg {box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;}li {box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;} ul {position: inherit; box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;}.body {box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;}.viewport {box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;}.body {background-color: #f1f1f1;display: flex;font-family: "Lato", sans-serif;font-size: 0.875rem;font-weight: 400;color: #2c3e50;overflow-y: hidden;}.chat::-webkit-scrollbar {width: 0.125rem;}.chat::-webkit-scrollbar-thumb {background: #cfd8dc;}.chat::-webkit-scrollbar-thumb:hover {background: #b0bec5;}/* INPUT TEXT PLACEHOLDER CUSTOMIZE */::-webkit-input-placeholder {color: #b0bec5;}::-moz-placeholder {color: #b0bec5;}:-ms-input-placeholder {color: #b0bec5;}:-moz-placeholder {color: #b0bec5;}#viewport {display: flex;flex: 1;justify-content: center;align-items: center;}#viewport > .chatbox {position: relative;display: table;float: left;margin: 1rem;width: 20rem;height: 36rem;background-color: white;box-shadow: 0 0.25rem 2rem rgba(38, 50, 56, 0.1);overflow: hidden;}#viewport > .chatbox > .chat {position: absolute;top: 0;left: 0;bottom: 0;right: 0;display: table-cell;vertical-align: bottom;padding: 1rem;margin-bottom: 2.875rem;overflow: auto;}.chatbox {position: relative;display: table;float: left;margin: 1rem;width: 30rem;height: 36rem;background-color: white;box-shadow: 0 0.25rem 2rem rgba(38, 50, 56, 0.1);overflow: hidden;}.chat {position: absolute;top: 0;left: 0;bottom: 0;right: 0;display: table-cell;vertical-align: bottom;padding: 1rem;margin-bottom: 2.875rem;overflow: auto;}ul {padding: 0;} li {position: relative;list-style: none;display: flow-root;margin-top: 1.5rem;margin: 1rem 0;transition: 0.5s all;} li:after {display: table;content: "";clear: both;}.msg {max-width: 85%;display: inline-block;padding: 0.5rem 1rem;line-height: 1rem;min-height: 2rem;font-size: 0.875rem;border-radius: 1rem;margin-bottom: 0.5rem;word-break: break-word;text-transform: none;}.msg.cliente {float: left;background-color: #e91e63;color: #fff;border-bottom-left-radius: 0.125rem;}.msg.mobo {float: right;background-color: #005d95;color: #fff;border-bottom-right-radius: 0.125rem;}span {font-weight: 500;position: absolute;}.partner {color: #b0bec5;font-size: 0.5rem;top: 0;font-size: 0.675rem;margin-top: -1rem;}.msg.cliente > span {left: 0;}.msg.mobo > span {right: 0;}</style>`;

export default function(data) {
  const messagesList = data.resource.items
    .map(item => {
      if (item.type === "application/vnd.lime.select+json") {
        item.content = item.content.text;
      }

      if (item.type === "application/vnd.lime.collection+json") {
        item.content = item.content.items[0].header.value.title;
      }

      if (item.type === "application/json") {
        item.content = item.metadata["#blip.payload.content"];
      }

      if (item.type === "text/plain") {
        item.content = item.metadata["#blip.payload.content"];
      }

      return item;
    })
    .filter(item => typeof item.content === "string")
    .map(item => {
      let sender = null;
      item.direction == "sent" ? (sender = "mobo") : (sender = "cliente");

      return `<li><div class="msg ${sender}"><span class="partner">${sender
        .charAt(0)
        .toUpperCase() + sender.slice(1)}</span>${item.content}</div></li>`;
    })
    .reverse();

  return `${styles}<div class="body"><div id="viewport"><div class="chatbox"><div class="chat"><ul>${messagesList.join(
    " "
  )}</ul></div></div></div></div>`;
}
