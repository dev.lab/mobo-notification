export default function(
  objArray = [],
  title = "",
  optionFieldName = "",
  optionValueNames = []
) {
  const options = objArray.map(obj => {
    let option = {};
    let values = {};
    option.text = obj[optionFieldName];

    if (optionValueNames instanceof Array) {
      optionValueNames.forEach(value => {
        values[value] = obj[value];
      });
      option.type = "application/json";
      option.value = values;
    }

    return option;
  });

  return {
    type: "application/vnd.lime.select+json",
    content: {
      scope: "immediate",
      text: title,
      options
    }
  };
}
