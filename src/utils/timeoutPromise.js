export default function(timeInMs, promise) {
  return new Promise(function(resolve, reject) {
    const timer = setTimeout(() => resolve("timeout"), timeInMs);
    promise
      .then(function(res) {
        clearTimeout(timer);
        resolve(res);
      })
      .catch(function(err) {
        clearTimeout(timer);
        reject(err);
      });
  });
}
