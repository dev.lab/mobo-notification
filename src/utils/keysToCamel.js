import {toCamel} from './StrFunctions'
import isObject from './isObject'

/**
 * Transform object keys to camel case
 * @param o
 * @returns {*}
 */
export default function keysToCamel (o) {
  if (isObject(o)) {
    const n = {}

    Object.keys(o)
      .forEach((k) => {
        n[toCamel(k)] = keysToCamel(o[k])
      })

    return n
  } else if (Array.isArray(o)) {
    return o.map((i) => {
      return keysToCamel(i)
    })
  }

  return o
}
