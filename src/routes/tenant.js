import { Router } from 'express';
import TenantRepository from '../repositories/TenantRepository'

const router = Router();

router.get('/:tenantId', async (req, res) => {
    const tenant = await TenantRepository.getByUuid(req.params.tenantId);
    res.send(tenant);
});

export default router;
