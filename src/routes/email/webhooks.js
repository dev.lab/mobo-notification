import { Router } from "express";

import EmailWebhookHttp from "../../controllers/http/emailWebhooks";

const router = Router({ mergeParams: true });

router.post("/", EmailWebhookHttp.post);

export default router;
