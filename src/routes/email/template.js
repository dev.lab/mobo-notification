import { Router } from "express";

import EmailTemplateHttp from "../../controllers/http/emailTemplate";

const router = Router({ mergeParams: true });

router.post("/", EmailTemplateHttp.post);

export default router;
