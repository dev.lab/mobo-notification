import { Router } from "express";
import template from "./template";
import webhooks from "./webhooks";

const router = Router({ mergeParams: true });

router.use("/template/:templateType", template);
router.use("/webhooks/:emailService", webhooks);

export default router;
