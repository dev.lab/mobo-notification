import express from "express";
import email from "./email";

import getTenantMiddleware from "../middleware/getTenant";
import authMiddleware from "../middleware/authMiddleware";
const router = express.Router();

router.use("/email/:tenantUuid", authMiddleware, getTenantMiddleware, email);

export default router;
