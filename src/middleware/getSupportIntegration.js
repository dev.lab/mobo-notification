import SupportIntegrationsRepository from "../repositories/SupportIntegrationsRepository";

export default async function getSupportIntegration(req, res, next) {
  req.supportIntegration = await SupportIntegrationsRepository.findByTenant(
    req.tenant.id
  );
  next();
}
