import TenantRepository from "../repositories/TenantRepository";

export default async function getTenant(req, res, next) {
  req.tenant = await TenantRepository.getByUuid(req.params);
  next();
}
