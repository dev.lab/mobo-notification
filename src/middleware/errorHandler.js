/**
 * Function to handle generic request errors.
 *
 * @param {*} err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function errorHandler(err, req, res, next) {
  let response = { message: err.message };
  if (err.code >= 100 && err.code < 600) res.status(err.code);
  else res.status(500);
  if (err.externalError) {
    response = {
      ...response,
      externalError: err.externalError.message
    };
  }
  res.send(response);
}

export default errorHandler;
