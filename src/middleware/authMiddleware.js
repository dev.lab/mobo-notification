
/**
 * Middleware that authenticate the user before each api call.
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const auth = (req, res, next) => {
    const authToken = req.headers.authorization;

    if (authToken === process.env.BLIP_CLIENT_API_KEY) {
        next();
    } else {
        res.status(403).send('Invalid token.');
    }
};

export default auth;