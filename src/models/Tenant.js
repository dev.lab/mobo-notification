import { Model, DataTypes } from "sequelize";


/**
 * Class that describes the model for tenant table.
 *
 * @class Tenant
 * @extends {Sequelize.Model}
 */
class Tenant extends Model {
    /**
     * Overrides Sequelize's parent Model init function
     *
     * @static
     * @param {*} sequelize
     * @returns
     * @memberof Tenant
     */
    static init(sequelize) {
        return super.init(
        {
            id: {
              primaryKey: true,
              type: DataTypes.INTEGER
            },
            name: DataTypes.STRING(81),
            uuid: DataTypes.STRING(36),
        },
        {
            sequelize,
            tableName: 'tenant',
            timestamps: false,
            freezeTableName: true,
        }
        );
    }

    static associate(models) {
      models.Tenant.hasMany(models.EmailAction);
    }
}

export default Tenant;
