import { Model, DataTypes } from "sequelize";

/**
 * Class that describes the model for emailaction table.
 *
 * @class EmailAction
 * @extends {Model}
 */
export default class EmailAction extends Model {
  /**
   * Overrides Sequelize's parent Model init function
   *
   * @static
   * @param {*} sequelize
   * @returns
   * @memberof EmailAction
   */
  static init(sequelize) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        version: DataTypes.INTEGER,
        template: DataTypes.STRING(40000),
        type: DataTypes.STRING(51),
        subject: DataTypes.STRING(100),
        tenant_id: DataTypes.INTEGER
      },
      {
        sequelize,
        tableName: "emailaction",
        underscored: true,
        underscoredAll: true,
        timestamps: false,
        freezeTableName: true
      }
    );
  }

  static associate(models) {
    models.EmailAction.belongsToMany(models.Contact, {
      through: models.EmailActionContact,
      foreignKey: "emailaction_id"
    });

    models.EmailAction.belongsTo(models.Tenant);
  }
}
