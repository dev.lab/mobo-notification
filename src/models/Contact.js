import { Model, DataTypes } from "sequelize";

/**
 * Class that describes the model for contact table.
 *
 * @class Contact
 * @extends {Model}
 */
export default class Contact extends Model {
  /**
   * Overrides Sequelize's parent Model init function
   *
   * @static
   * @param {*} sequelize
   * @returns
   * @memberof Contact
   */
  static init(sequelize) {
    return super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        version: DataTypes.INTEGER,
        email: DataTypes.STRING(101)
      },
      {
        sequelize,
        tableName: "contact",
        underscored: true,
        underscoredAll: true,
        timestamps: false,
        freezeTableName: true
      }
    );
  }

  static associate(models) {
    models.Contact.belongsToMany(models.EmailAction, {
      through: models.EmailActionContact,
      foreignKey: "contacts_id"
    });
  }
}
