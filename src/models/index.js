import { Sequelize } from "sequelize";

import Tenant from "./Tenant";
import EmailAction from "./EmailAction";
import Contact from "./Contact";
import EmailActionContact from "./EmailActionContact";
import debugLib from 'debug';

const debug = debugLib('mobo-notification:sql');

const sequelize = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_CONNECTION,
    logging: debug
  }
);

const models = {
  Tenant: Tenant.init(sequelize) ,
  EmailAction: EmailAction.init(sequelize),
  Contact: Contact.init(sequelize),
  EmailActionContact: EmailActionContact.init(sequelize),
};

/**
 * Tables that has associations are called here
 */
Object.keys(models).forEach(modelName => {
  if (models[modelName].associate) {
    models[modelName].associate(models);
  }
});

export { sequelize };

export default models;
