import { Model, DataTypes } from "sequelize";

/**
 * Class that describes the model for contact table.
 *
 * @class Contact
 * @extends {Model}
 */
export default class EmailActionContact extends Model {
  /**
   * Overrides Sequelize's parent Model init function
   *
   * @static
   * @param {*} sequelize
   * @returns
   * @memberof Contact
   */
  static init(sequelize) {
    return super.init(
      {
        emailaction_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: "EmailAction",
            key: "id",
            as: "emailaction_id",
          },
        },
        contacts_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: "Contacts",
            key: "id",
            as: "contacts_id",
          },
        },
      },
      {
        sequelize,
        tableName: "emailaction_contact",
        timestamps: false,
        underscored: true,
        underscoredAll: true,
        freezeTableName: true,
      }
    );
  }
}
