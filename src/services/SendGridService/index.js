// import processResponse from "./Traits/processResponse";
// import { ExternalDependency } from "../../errors";
import sgMail from "@sendgrid/mail";
import { ExternalDependency } from "../../errors";

/**
 * Class to handle Twilio SendGrid for
 * transactional emails
 * @class SendGridService
 */
export default class SendGridService {
  constructor() {
    this.sgMail = sgMail;
    this.sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    this.message = {};
  }
  /**
   * set parameters for a html templated message
   * @param {string} to Recipient email address
   * @param {string} cc Copy (optional)
   * @param {string} bcc Hidden Copy (optional)
   * @param {string} from who is sending the message
   * @param {string} subject Email subject
   * @param {string} html Template html of the message
   */
  setHtmlMessage({ to, cc, bcc, from, subject, html }) {
    from = from || process.env.DEFAULT_SENDER;
    this.message = {
      to,
      cc,
      bcc,
      from,
      subject,
      html,
    };
    return this;
  }

  /**
   * set parameters for a plain text message
   * @param {string} to Recipient email address
   * @param {string} cc Copy (optional)
   * @param {string} bcc Hidden Copy (optional)
   * @param {string} from who is sending the message
   * @param {string} subject Email subject
   * @param {string} text Plain text
   */
  setTextMessage({ to, cc, bcc, from, subject, text }) {
    from = from || process.env.DEFAULT_SENDER;
    this.message = {
      to,
      cc,
      bcc,
      from,
      subject,
      text,
    };

    return this;
  }

  /**
   * Adds attachments to the message
   * @param {Array} attachments
   */
  setAttachments(attachments) {
    this.message.attachments = attachments;
    return this;
  }

  /**
   * Send the email
   */
  async send() {
    try {
      sgMail.send(this.message);
    } catch (error) {
      throw new ExternalDependency(error);
    }
  }
}
