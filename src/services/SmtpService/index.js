import nodemailer from "nodemailer";

/**
 * Class to handle Gmail email send
 * @class SmtpService
 */
export default class SmtpService {
  constructor() {
    this.mail = nodemailer.createTransport({
      host: "smtp.googlemail.com", // Gmail Host
      port: 465, // Port
      secure: true, // this is true as port is 465
      auth: {
        user: process.env.DEFAULT_SENDER, //Gmail username
        pass: process.env.DEFAULT_SENDER_PASSWORD, // Gmail password
      },
    });
    this.message = {};
  }
  /**
   * set parameters for a html templated message
   * @param {string} to Recipient email address
   * @param {string} cc Copy (optional)
   * @param {string} bcc Hidden Copy (optional)
   * @param {string} from who is sending the message
   * @param {string} subject Email subject
   * @param {string} html Template html of the message
   */
  setHtmlMessage({ to, cc, bcc, from, subject, html }) {
    from = from || process.env.DEFAULT_SENDER;
    this.message = {
      from,
      to: to.map((emailTo) => emailTo.email).join(","), // Recepient email address. Multiple emails can send separated by commas
      cc: cc.map((emailTo) => emailTo.email).join(","), // Recepient email address. Multiple emails can send separated by commas
      bcc: bcc.map((emailTo) => emailTo.email).join(","), // Recepient email address. Multiple emails can send separated by commas
      subject,
      html,
    };
    return this;
  }

  /**
   * set parameters for a plain text message
   * @param {string} to Recipient email address
   * @param {string} cc Copy (optional)
   * @param {string} bcc Hidden Copy (optional)
   * @param {string} from who is sending the message
   * @param {string} subject Email subject
   * @param {string} text Plain text
   */
  setTextMessage({ to, cc, bcc, from, subject, text }) {
    from = from || process.env.DEFAULT_SENDER;
    this.message = {
      from,
      to: to.map((emailTo) => emailTo.email).join(","), // Recepient email address. Multiple emails can send separated by commas
      cc: cc.map((emailTo) => emailTo.email).join(","), // Recepient email address. Multiple emails can send separated by commas
      bcc: bcc.map((emailTo) => emailTo.email).join(","), // Recepient email address. Multiple emails can send separated by commas
      subject,
      text,
    };

    return this;
  }

  /**
   * Adds attachments to the message
   * @param {Array} attachments
   */
  setAttachments(attachments) {
    this.message.attachments = attachments;
    return this;
  }

  /**
   * Send the email
   */
  async send() {
    this.mail.sendMail(this.message, (error) => {
      if (error) {
        return console.log(error);
      }
      debug("Message sent.");
    });
    return this.mail.sendMail(this.message);
  }
}
