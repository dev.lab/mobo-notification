import { ExternalDependency } from "../../errors";
import fs from "fs";
import path from "path";
import SendGridService from "../SendGridService";
import SmtpService from "../SmtpService";
/**
 * Class to handle transactional emails
 * @class EmailServiceFactory
 */

const SENDGRID_MAILSERVICE = 'SendGrid';
const SMTP_MAILSERVICE = 'Smtp';

class EmailServiceFactory {
  constructor(mailService) {
    this.mailService = this.makeService(mailService);
    this.sender = process.env.DEFAULT_SENDER;
    this.message = {};
  }

  makeService (mailService) {
    switch (mailService) {
      case SENDGRID_MAILSERVICE:
          return new SendGridService();
      case SMTP_MAILSERVICE:
      default:
          return new SmtpService();
    }
  }

  /**
   * set parameters for a html templated message
   * @param {string} to Recipient email address
   * @param {string} cc Copy (optional)
   * @param {string} bcc Hidden Copy (optional)
   * @param {string} from who is sending the message
   * @param {string} subject Email subject
   * @param {string} html Template html of the message
   */
  setHtmlMessage({ to, cc, bcc, from, subject, html }) {
    this.mailService.setHtmlMessage({ to, cc, bcc, from, subject, html });
    this.addIcons();
    return this;
  }

  /**
   * set parameters for a plain text message
   * @param {string} to Recipient email address
   * @param {string} cc Copy (optional)
   * @param {string} bcc Hidden Copy (optional)
   * @param {string} from who is sending the message
   * @param {string} subject Email subject
   * @param {string} text Plain text
   */
  setTextMessage({ to, cc, bcc, from, subject, text }) {
    this.mailService.setTextMessage({ to, cc, bcc, from, subject, text });
    return this;
  }

  /**
   * Gets an image file object from resources directory
   * @param {string} name Image name like test.png
   * @returns {object} Contains filename, type, content_id, content (base64) and disposition (inline)
   */
  getImage(name) {
    const [_, extension] = name.split(".");
    return {
      filename: `${name}`,
      type: `image/${extension}`,
      contentType: `image/${extension}`,
      content_id: `${name}`,
      cid: `${name}`,
      content: fs.readFileSync(path.resolve("src", "resources", name), {
        encoding: "base64",
      }),
      encoding: "base64",
      disposition: "inline",
    };
  }

  /**
   * Add specific icon set (images on resources) on emails
   */
  addIcons() {
    const icons = fs.readdirSync(path.resolve("src", "resources"));
    const attachments = icons
      .filter((icon) => this.mailService.message.html.includes(icon))
      .map((icon) => this.getImage(icon));
    this.mailService.setAttachments(attachments);
    return this;
  }
  /**
   * Sends the email message
   */
  async send() {
    try {
      this.mailService.send(this.mailService.message);
    } catch (error) {
      throw new ExternalDependency(error);
    }
  }
}

export default function createEmailService(mailService) {
  return new EmailServiceFactory(mailService);
};
