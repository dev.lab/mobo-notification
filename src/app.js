// app.js
import 'dotenv/config';
import express from 'express';
import path from 'path';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import router from './routes/index';
import errorHandler from './middleware/errorHandler';
import auth from './middleware/authMiddleware';

const app = express();

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.use(auth);

app.use('/api/v1', router);

app.use(errorHandler);

export default app;
