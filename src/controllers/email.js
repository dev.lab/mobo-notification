import moment from "moment";

import EmailActionRepository from "../repositories/EmailActionRepository";

import createEmailService from "../services/EmailService";

import templating from "../utils/templating.js";
export default class EmailController {
  static formatReceivers({ Contacts }) {
    const init = { to: [], cc: [], bcc: [{ email: process.env.BACKUP_MAIL }] };
    return Contacts.reduce((receivers, currentContact) => {
      receivers.to.push({ email: currentContact.email });
      return receivers;
    }, init);
  }

  static async sendEmailToTenant({ tenant, templateType, templateVariables }) {
    const defaultVariables = {
      _datetimeFormatted: moment().format("DD/MM/YYYY HH:mm"),
    };

    const emailActionData = await EmailActionRepository.getTenantEmailTemplate({
      tenantId: tenant.id,
      templateType,
    });

    templateVariables = { ...templateVariables, ...defaultVariables };

    const emailData = {
      html: templating(emailActionData.template, templateVariables),
      subject: `[${tenant.name}] - ${emailActionData.subject}`,
    };

    const emailReceivers = EmailController.formatReceivers(emailActionData);

    const emailConfig = {
      ...emailData,
      ...emailReceivers,
      substitutions: templateVariables,
    };

    const emailService = createEmailService('sendgrid');
    return emailService.setHtmlMessage(emailConfig).send();
  }
}
