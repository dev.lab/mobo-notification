export default class EmailWebhookHttp {
  static async post(req, res, next) {
    const { emailService } = req.params;
    try {
      console.log('req.body', req.body);
      res.json(emailService);
    } catch (error) {
      next(error);
    }
  }
}


