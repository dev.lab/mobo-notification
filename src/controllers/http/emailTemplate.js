import EmailController from "../../controllers/email.js";
import debugLib from "debug";

export default class EmailTemplateHttp {
  static async post(req, res, next) {
    const debug = debugLib('mobo-notification:http');
    const { templateType } = req.params;
    const templateVariables = req.body;

    debug("Body:", templateVariables);

    try {
      const emailResponse = await EmailController.sendEmailToTenant({
        tenant: req.tenant,
        templateType,
        templateVariables,
      });

      res.json(emailResponse);
    } catch (error) {
      next(error);
    }
  }
}


