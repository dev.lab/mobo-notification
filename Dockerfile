FROM node:lts-alpine
RUN mkdir /app
COPY . /app
RUN chown -R node:node /app
USER node
EXPOSE 3000
WORKDIR /app
RUN cp .env.production .env
RUN npm install --production
CMD ["node", "dist/bin/www"]
